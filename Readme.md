The algorithm for smoking detection is as follows:  
	- detect a short movement over a certain speed threshold (done using the gyroscope)  
	- detect if the hand becomes stationary within a short period of time after this movement  
	- detect if the hand is in a potential smoking position using the accelerometer (decision tree using training data)  
	- if all of these hold the drag has started  
	- the drag stops when a movement over a certain threshold is detected  
	- if the drag is too short it is discounted, if it is too long the whole smoking session is discounted  
	- this repeats  
	- no longer counted as smoking if no drag has occurred within a certain time frame (80 seconds)  
	- when the smoking stops if the number of drags is below a threshold (6) it is discounted  

Currently the app on the watch shows if it thinks the wearer is currently smoking, the number of drags of the current
or most recent cigarette and the number of cigarettes the watch as counted today. This information is stored in a
database on the watch along with the predicted start and end time of each drag (in order to calculate the length of
each drag and the intervals between them).

All the code for the app is currently in the wear folder under wear/src/main/java/com/example/hazel/smokingtracker.
MainActivity implements the algorithm outlined above and the related additions, deletions and queries to the database.
SmokingContract outlines the format of the database.
SmokingtrackerDbHelper contains functions to create, delete the database along with how the database should deal with 
upgrades and downgrades of the app's version.

The results have so far seemed positive (in Results.xlsx), although it is obvious some improvement is needed, the pilot 
testing flagged up a couple of things that could be improved:  
	- the hand position detection needs to be slightly more accurate. Currently there are a few cases not picked up
	  on by the watch. More training data is needed for this. On the arduino this was done by means of a random forest,
	  I got better results on the watch with just one decision tree. It is likely with a bit more data the performance
	  of the random forest would be more suitable and improve the accuracy of the watch  
	- the moving and stationary parameters need to be tested thoroughly, there were a few cases in testing where the 
	  hand to mouth motion could have been too slow to be picked up as a smoking motion by the watch and also a few 
	  cases were a slight movement caused the drag to end early. This is not as bad as it sounds as there is quite a 
	  large interval between the thresholds for movement and stationary (i.e. a period considered slow movement).  
	  
There are also a few improvements I can think of that were not included in the original Arduino code:  
	- linking up to the phone so we can display interface on the phone to see details of smoking sessions  
	- it's possible the issue with slight movement making a drag too short could be solved by making the movement away  
	  last a certain time before the drag is stopped  
	- the app should be constantly running in the background on the watch  
    - the power consumption would need to be looked into so the watch lasts all day  
	