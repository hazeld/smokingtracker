package com.example.hazel.smokingtracker;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.hazel.smokingtracker.SmokingContract.CigaretteEntry;
import com.example.hazel.smokingtracker.SmokingContract.DragEntry;
/**
 * Created by Hazel on 02/06/2015.
 */
public class SmokingTrackerDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String SQL_CREATE_TABLE_CIGARETTE =
            "CREATE TABLE " + CigaretteEntry.TABLE_NAME + " (" +
                    CigaretteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    CigaretteEntry.COLUMN_NAME_DATE + TEXT_TYPE + " )";

    private  static  final String SQL_CREATE_TABLE_DRAG =
            "CREATE TABLE " + DragEntry.TABLE_NAME + " (" +
                    DragEntry._ID + " INTEGER PRIMARY KEY," +
                    DragEntry.COLUMN_NAME_CIGARETTE_ID + " INTEGER," +
                    DragEntry.COLUMN_NAME_START_TIME + " INTEGER," +
                    DragEntry.COLUMN_NAME_END_TIME + " INTEGER" + " )";

    private static final String SQL_DELETE_TABLE_CIGARETTE =
            "DROP TABLE IF EXISTS " + CigaretteEntry.TABLE_NAME;

    private static final String SQL_DELETE_TABLE_DRAG =
            "DROP TABLE IF EXISTS " + DragEntry.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SmokingTracker.db";

    public SmokingTrackerDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_CIGARETTE);
        db.execSQL(SQL_CREATE_TABLE_DRAG);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE_CIGARETTE);
        db.execSQL(SQL_DELETE_TABLE_DRAG);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
