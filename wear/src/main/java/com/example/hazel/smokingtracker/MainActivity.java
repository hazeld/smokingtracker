package com.example.hazel.smokingtracker;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import java.lang.Math;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.example.hazel.smokingtracker.SmokingContract.CigaretteEntry;
import com.example.hazel.smokingtracker.SmokingContract.DragEntry;

public class MainActivity extends Activity implements SensorEventListener {

    private TextView mTextView;
    private static final String TAG = "MainActivity";
    private double movingThreshold = 1.5;
    private double stationaryThreshold = 0.6;
    private float accelRange;
    private boolean bDrag = false;
    private long timeMoving = 0;
    private long timeSinceMoved = 0;
    private long oldTimeAccel;
    private long oldTimeGyro;
    private boolean bHandMoved = false;
    private int smokingPos = 0;
    private boolean bSmoking = false;
    private double minDragLength = 0.6;
    private double maxDragLength = 5.5;
    private int minDrags = 6;
    private double maxInterval = 80.0;

    //Deals with starting the app
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                //writes info to the screen
                mTextView = (TextView) stub.findViewById(R.id.cigNumber);
                SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                int count = findCigsToday(db);
                mTextView.setText("" + count);
                db.close();
            }
        });
        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
        oldTimeAccel = seconds;
        oldTimeGyro = seconds;
        getGyroData();
        getAccelData();
    }

    //gets the accelerometer data from the watch
    private void getAccelData() {
        SensorManager mSensorManager = ((SensorManager)getSystemService(SENSOR_SERVICE));
        Sensor mAccelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelSensor, SensorManager.SENSOR_DELAY_NORMAL);
        accelRange = mAccelSensor.getMaximumRange();
    }

    //gets the gyroscope data from the watch
    private void getGyroData() {
        SensorManager mSensorManager = ((SensorManager)getSystemService(SENSOR_SERVICE));
        Sensor mGyroscopeSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mSensorManager.registerListener(this, mGyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    //deals with when the accuracy of the sensors is changed -- must have
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged - accuracy: " + accuracy);
    }

    //when the values from one of the sensors has changed
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            //Log.d("gyro", event.timestamp + "\t" + event.values[0] + "\t" + event.values[1] + "\t" + event.values[2]);
            int handMoving = checkHandMoving(event.values[0], event.values[1], event.values[2]);
            //if(handMoving == 2)
                //Log.d("gyro", "Moving");
            //else if(handMoving == 0)
                //Log.d("gyro", "Stationary");
            bHandMoved = checkMovementPhase(handMoving, event.timestamp, oldTimeGyro);

            //if in a smoking position, stationary with a recent movement phase start the drag if not already
            //if in a drag and move away end the drag
            if(smokingPos == 1 && !bDrag && handMoving == 0 && bHandMoved) {
                resetMovingVars();
                startDrag(event.timestamp);
            }
            else if(bDrag && (handMoving == 2 || handMoving == 1)) {
                endDrag(event.timestamp);
            }

            //stop smoking if been too long since a drag
            if(bSmoking && !bDrag) {
                long dragEndTime = getDragTime(DragEntry.COLUMN_NAME_END_TIME);
                long sinceDrag = event.timestamp - dragEndTime;
                if(sinceDrag > maxInterval *  1000000000) {
                    stopSmoking(false);
                }
            }

            oldTimeGyro = event.timestamp;
        }
        else if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            //Log.d("accel", event.timestamp + "\t" + event.values[0] + "\t" + event.values[1] + "\t" + event.values[2]);
            smokingPos = checkHandPos(event.values[0], event.values[1], event.values[2]);
            if(smokingPos != 0)
                Log.d("accel", "Smoking Position");

            oldTimeAccel = event.timestamp; //TODO: maybe don't need
        }
    }

    //adds drag to database
    private void startDrag(long time) {
        if(!bSmoking)
            startSmoking();
        bDrag = true;
        //insert new drag with start time
        SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        long cigID = getLastEntryID(db, CigaretteEntry.TABLE_NAME, CigaretteEntry._ID);
        values.put(DragEntry.COLUMN_NAME_CIGARETTE_ID, cigID);
        values.put(DragEntry.COLUMN_NAME_START_TIME, time);
        db.insert(DragEntry.TABLE_NAME, null, values);

        Log.d("smoking", "Started drag");
        if(bSmoking) {
            long dragStartTime = getDragTime(DragEntry.COLUMN_NAME_START_TIME);
            long dragEndTime = getPreviousDragEnd();
            long dragInterval = dragStartTime - dragEndTime;
            long dragInterval2 = getDragInterval(db, getLastEntryID(db, DragEntry.TABLE_NAME, DragEntry._ID));
            //Log.d("check", "Method 1: " + dragInterval + " Method 2: " + dragInterval2);
        }
        db.close();
    }

    //adds cigarette to database
    private void startSmoking() {
        bSmoking = true;
        //get current date
        ContentValues values = new ContentValues();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        values.put(CigaretteEntry.COLUMN_NAME_DATE, dateFormat.format(new Date()));
        //add cigarette
        SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.insert(CigaretteEntry.TABLE_NAME, null, values);

        //show on app screen
        TextView view = (TextView) findViewById(R.id.text);
        view.setText("Smoking");
        db.close();
    }

    //decides if movement is a drag updates the database accordingly
    private void endDrag(long time) {
        bDrag = false;
        long dragStartTime = getDragTime(DragEntry.COLUMN_NAME_START_TIME);
        long length = time - dragStartTime;
        if(length < minDragLength *  1000000000) {
            Log.d("smoking", "Drag discontinued, too short");
            //removes last drag
            SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
            SQLiteDatabase db = mDbHelper.getWritableDatabase();

            long rowid = getLastEntryID(db, DragEntry.TABLE_NAME, DragEntry._ID);

            db.delete(DragEntry.TABLE_NAME, DragEntry._ID + "=" + rowid, null);
            db.close();
        }
        else if(length > maxDragLength * 1000000000) {
            Log.d("smoking", "Drag too long, not smoking " + length + " " + maxDragLength*100000000);
            stopSmoking(true);
        }
        else {
            addDragEnd(time);
            SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            long rowid = getLastEntryID(db, DragEntry.TABLE_NAME, DragEntry._ID);
            long dragLength = getDragLength(db, rowid);
            Log.d("smoking", "Drag ended, time: " + dragLength);

            //show number of drags so far
            int count = getNoDrags(db, getLastEntryID(db, CigaretteEntry.TABLE_NAME, CigaretteEntry._ID));
            TextView view = (TextView) findViewById(R.id.dragNumber);
            view.setText("" + count);
            db.close();
        }
    }

    //adds the end time to a finished drag
    private void addDragEnd(long endTime) {
        SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //new value for end time column
        ContentValues values = new ContentValues();
        values.put(DragEntry.COLUMN_NAME_END_TIME, endTime);
        //Which row to update based on ID
        long rowid = getLastEntryID(db, DragEntry.TABLE_NAME, DragEntry._ID);
        String selection = DragEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(rowid) };

        //count should be one
        int count = db.update(DragEntry.TABLE_NAME, values, selection, selectionArgs);
        if(count == 0)
            Log.d("db", "Problem: drag not updated");
        else if(count > 1)
            Log.d("db", "Problem: too many drags updated");
        db.close();
    }

    //updates screen when stopped smoking and removes the cigarette and associated drags if removeAll
    //is true
    private void stopSmoking(boolean removeAll) {
        SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        long cigID = getLastEntryID(db, CigaretteEntry.TABLE_NAME, CigaretteEntry._ID);
        int noDrags = getNoDrags(db, cigID);
        if((noDrags >= minDrags) && !removeAll) {
            Log.d("smoking", "Smoking stoppped, no drags: " + noDrags);
        }
        else {
            //removes the previously added cigarette
            long rowid = getLastEntryID(db, CigaretteEntry.TABLE_NAME, CigaretteEntry._ID);

            db.delete(CigaretteEntry.TABLE_NAME, CigaretteEntry._ID + "=" + rowid,null);

            //remove associated drags
            db.delete(DragEntry.TABLE_NAME, DragEntry.COLUMN_NAME_CIGARETTE_ID + "=" + rowid, null);
        }
        bSmoking = false;

        //Show not smoking
        TextView view = (TextView) findViewById(R.id.text);
        view.setText("Not Smoking");

        int count = findCigsToday(db);
        TextView view2 = (TextView) findViewById(R.id.cigNumber);
        view2.setText("" + count);

        db.close();
    }

    //finds the number of cigarettes smoked today
    private int findCigsToday(SQLiteDatabase db) {
        //get date
        ContentValues values = new ContentValues();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());

        String projection[] = {CigaretteEntry._ID, CigaretteEntry.COLUMN_NAME_DATE};
        String sortOrder = CigaretteEntry._ID + " DESC";

        String selection = CigaretteEntry.COLUMN_NAME_DATE + " LIKE ?";
        String[] selectionArgs = {date};

        //first query for the more recent start time
        Cursor c = db.query(CigaretteEntry.TABLE_NAME, projection, selection, selectionArgs, null, null,
                sortOrder);

        int count = 0;
        try {
            c.moveToFirst();
            count = c.getCount();
        }
        finally {
            c.close();
        }
        return count;
    }

    //gets the ID of the last entry in a specified table
    private long getLastEntryID(SQLiteDatabase db, String tableName, String IDCol) {
        Cursor cursor = db.rawQuery("Select * from " + tableName, null);

        long id = -1;
        try {
            cursor.moveToLast();
            id = cursor.getLong(cursor.getColumnIndex(IDCol));
        }
        finally {
            cursor.close();
        }
        return id;
    }

    //get the previous drag's end time
    private long getPreviousDragEnd() {
        SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        long cigrowID = getLastEntryID(db, CigaretteEntry.TABLE_NAME, CigaretteEntry._ID);

        String[] projection = {DragEntry._ID, DragEntry.COLUMN_NAME_CIGARETTE_ID};
        String sortOrder = DragEntry._ID + " DESC";
        String selection = DragEntry.COLUMN_NAME_CIGARETTE_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(cigrowID) };

        Cursor c = db.query(DragEntry.TABLE_NAME, projection, selection, selectionArgs, null, null,
                sortOrder);

        int count = 0;
        try {
            c.moveToFirst();
            count = c.getCount();
        }
        finally {
            c.close();
        }

        long result = 0;
        if(count > 1) {
            long rowid = getLastEntryID(db, DragEntry.TABLE_NAME, DragEntry._ID);
            rowid = rowid - 1;
            result = getDragTime(db, DragEntry.COLUMN_NAME_END_TIME, rowid);
        }
        db.close();
        return result;
    }

    //get the length of time between the most recent drag and the previous one
    private long getDragInterval(SQLiteDatabase db, long dragID) {
        String projection1[] = {DragEntry._ID, DragEntry.COLUMN_NAME_START_TIME};
        String sortOrder = DragEntry._ID + " DESC";

        String selection = DragEntry._ID + " LIKE ?";
        String[] selectionArgs1 = { String.valueOf(dragID)};

        //first query for the more recent start time
        Cursor c1 = db.query(DragEntry.TABLE_NAME, projection1, selection, selectionArgs1, null, null,
                sortOrder);

        //second query for the previous end time
        String projection2[] = {DragEntry._ID, DragEntry.COLUMN_NAME_END_TIME};
        dragID--;
        String[] selectionArgs2 = {String.valueOf(dragID)};

        Cursor c2 = db.query(DragEntry.TABLE_NAME, projection2, selection, selectionArgs2, null, null,
                sortOrder);

        long startTime = 0;
        long endTime = 0;
        try {
            c1.moveToFirst();
            c2.moveToFirst();
            startTime = c1.getLong(c1.getColumnIndex(DragEntry.COLUMN_NAME_START_TIME));
            endTime = c2.getLong(c2.getColumnIndex(DragEntry.COLUMN_NAME_END_TIME));
        }
        finally {
            c1.close();
            c2.close();
        }

        return startTime - endTime;
    }

    //get the length of a drag with the specified ID
    private long getDragLength(SQLiteDatabase db, long dragID) {
        String projection[] = {DragEntry._ID, DragEntry.COLUMN_NAME_START_TIME,
                DragEntry.COLUMN_NAME_END_TIME};
        String sortOrder = DragEntry._ID + " DESC";

        String selection = DragEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(dragID)};

        Cursor c = db.query(DragEntry.TABLE_NAME, projection, selection, selectionArgs, null, null,
                sortOrder);

        long startTime = 0;
        long endTime = 0;

        try {
            c.moveToFirst();
            startTime = c.getLong(c.getColumnIndex(DragEntry.COLUMN_NAME_START_TIME));
            endTime = c.getLong(c.getColumnIndex(DragEntry.COLUMN_NAME_END_TIME));
        }
        finally {
            c.close();
        }
        return endTime - startTime;
    }

    //get the number of drags in a cigarette specified by the cigarette ID
    private int getNoDrags(SQLiteDatabase db, long cigID) {
        String projection[] = {DragEntry._ID};
        String sortOrder = DragEntry._ID + " DESC";

        String selection = DragEntry.COLUMN_NAME_CIGARETTE_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(cigID)};

        Cursor c = db.query(DragEntry.TABLE_NAME, projection, selection, selectionArgs, null, null,
                sortOrder);

        int count = 0;
        try {
            c.moveToFirst();
            count = c.getCount();
        }
        finally {
            c.close();
        }
        return count;
    }

    //get start time or end time of the most recent drag
    private long getDragTime(String colName) {
        SmokingTrackerDbHelper mDbHelper = new SmokingTrackerDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        long rowid = getLastEntryID(db, DragEntry.TABLE_NAME, DragEntry._ID);
        long result = getDragTime(db, colName, rowid);
        db.close();
        return result;
    }

    //get the start or end time of a drag specified by rowid
    private long getDragTime(SQLiteDatabase db, String colName, long rowid) {
        String projection[] = {colName};
        String sortOrder = colName + " DESC";

        String selection = DragEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(rowid) };

        Cursor c = db.query(DragEntry.TABLE_NAME, projection, selection, selectionArgs, null, null,
                sortOrder);

        long time = 0;
        try {
            c.moveToFirst();
            time = c.getLong(c.getColumnIndex(colName));
        }
        finally {
            c.close();
        }
        return time;
    }

    //checks whether the hand position is in a potential smoking position
    private int checkHandPos(float x1, float x2, float x3) {
        x1 *=(20000/accelRange);
        x2 *=(20000/accelRange);
        x3 *=(20000/accelRange);
        //Log.d("accel", x1 + " " + x2 + " " + x3);
        int sum = 0;
        if(x1<-7383.5 ) {
            if(x3<-2723.5 ) {
                sum = sum;
            }
            else {
                if(x3<5753.5 ) {
                    if(x1<-10436.5 ) {
                        sum = sum;
                    }
                    else {
                        if(x2<-6574 ) {
                            sum = sum;
                        }
                        else {
                            if(x3<5129.5 ) {
                                sum++;
                            }
                            else {
                                if(x1<-8261 ) {
                                    sum = sum;
                                }
                                else {
                                    sum++;
                                }
                            }
                        }
                    }
                }
                else {
                    sum = sum;
                }
            }
        }
        else {
            sum = sum;
        }
        return sum;
    }

    //checks the movement phase the hand is in
    private boolean checkMovementPhase(int moving, long newTime, long oldTime) {
        if(!bDrag) {
            if ( timeMoving < 300000000) { //TODO: need to get a better upper limit
                if(moving == 2) {
                    timeMoving = timeMoving + (newTime - oldTime);
                }
                else {
                    timeMoving = 0;
                }
            }
            else {
                if(timeSinceMoved < 700000000) {
                    timeSinceMoved = timeSinceMoved + (newTime - oldTime);
                    return true;
                }
                else {
                    resetMovingVars();
                }
            }
        }
        return false;
    }

    //resets the variable related to hand movement
    private void resetMovingVars() {
        timeSinceMoved = 0;
        timeMoving = 0;
    }

    //checks if the hand is moving
    private int checkHandMoving(float x, float y, float z) {
        float sum = Math.abs(x) + Math.abs(y) + Math.abs(z);
        if(sum > movingThreshold) {
            return 2;
        }
        else if(sum < stationaryThreshold) {
            return 0;
        }
        else {
            return 1;
        }
    }

}
