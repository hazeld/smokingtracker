package com.example.hazel.smokingtracker;

import android.provider.BaseColumns;
/**
 * Created by Hazel on 02/06/2015.
 */
public final class SmokingContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public SmokingContract() {}

    public static abstract class CigaretteEntry implements BaseColumns {
        public static final String TABLE_NAME = "cigarettes";
        public static final String COLUMN_NAME_DATE = "date";
    }

    public static abstract class DragEntry implements BaseColumns {
        public static final String TABLE_NAME = "drags";
        public static final String COLUMN_NAME_CIGARETTE_ID = "cigaretteid";
        public static final String COLUMN_NAME_START_TIME = "starttime";
        public static final String COLUMN_NAME_END_TIME = "endtime";
    }
}
